<?php
/**
 * Created by 惠达浪
 * Email: crazys@126.com
 * Date: 2018/11/28
 * Time: 12:39
 */

namespace app\lib\exception;


use app\lib\enum\ErrorCodeEnum;

class TokenException extends BaseException {
    public function __construct($code = 403, $message = 'Token无效', $errorCode = ErrorCodeEnum::TOKEN_INVALID) {
        parent::__construct($code, $message, $errorCode);
    }
}