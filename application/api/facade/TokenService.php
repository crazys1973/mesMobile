<?php
/**
 * Created by 惠达浪
 * Email: crazys@126.com
 * Date: 2018/11/29
 * Time: 11:04
 */

namespace app\api\facade;

use think\Facade;

/**
 * @see     \app\api\service\Token
 * @mixin \app\api\service\Token
 * @method \Lcobucci\JWT\Token generateToken(array $claims) static 生成令牌
 * @method mixed getTokenVar($token, $key) static 根据token获取指定键对应的信息
 * @method mixed getUserIdByToken($token) static 使用令牌换取用户当前id
 * @method mixed getWorkcenterIdByToken($token) static 使用令牌获取工作中心标识
 * @method bool|int verifyToken($token) static 检验token，成功返回true，失败返回错误码
 * @package app\api\facade
 */
class TokenService extends Facade {
    /**
     * 获取当前Facade对应类名（或者已经绑定的容器对象标识）
     * @access protected
     * @return string
     */
    protected static function getFacadeClass() {
        return 'app\api\service\Token';
    }
}