<?php
/**
 * Created by PhpStorm.
 * User: crazy
 * Date: 2018/12/12
 * Time: 11:33
 */

namespace app\api\validate;

/**
 * 手机端相关验证
 *
 * @package app\api\validate
 */
class MobileValidate extends BaseValidate {
    protected $rule = [
        'id' => 'require|number',
        'category_id|文章分类id' => 'require|number',
        'page|分页参数page' => 'require|number'
    ];
    protected $scene = [
        'article' => ['id'],
        'list' => ['category_id', 'page']
    ];
}