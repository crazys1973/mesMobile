<?php
/**
 * Created by 惠达浪
 * Email: crazys@126.com
 * Date: 2018/11/25
 * Time: 11:26
 */

namespace app\api\validate;

/**
 * Mes相关的验证
 *
 * @package app\api\validate
 */
class MesValidate extends BaseValidate {
    protected $rule = [
        'planid|工单id' => 'require|number',
        'result_id|任务id' => 'require|number',
        'userIds|用户群id' => 'require',
        'quantity|报产产量' => 'require|number'
    ];

    protected $message = [
        'planid.number' => 'planid必须是正整数'
    ];

    protected $scene = [
        'planid' => ['planid'],
        'start' => ['result_id', 'userIds'],
        'report' => ['result_id', 'userIds', 'quantity']
    ];
}