<?php
/**
 * Created by 惠达浪
 * Email: crazys@126.com
 * Date: 2018/12/3
 * Time: 17:05
 */

namespace app\api\model;

use think\Model;

class Attendance extends Model {
    public $autoWriteTimestamp = false;
    protected $type = [
        'attendance_time' => 'timestamp',
        'attendance_status' => 'integer'
    ];
}