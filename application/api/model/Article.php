<?php
/**
 * Created by PhpStorm.
 * User: crazy
 * Date: 2018/12/12
 * Time: 21:10
 */

namespace app\api\model;

use think\Model;

class Article extends Model {
    protected $createTime = 'art_publishtime';
    protected $updateTime = false;
}