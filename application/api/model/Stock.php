<?php
/**
 * Created by 惠达浪
 * Email: crazys@126.com
 * Date: 2018/12/2
 * Time: 18:35
 */

namespace app\api\model;

use think\Model;

/**
 * stock 库房模型
 * @package app\api\model
 */
class Stock extends Model {
    protected $hidden = ['create_time', 'update_time'];
}