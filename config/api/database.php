<?php
/**
 * Created by 惠达浪
 * Email: crazys@126.com
 * Date: 2018/11/29
 * Time: 20:50
 */

return [
    // 数据库类型
    'type' => 'mysql',
    // 服务器地址
    'hostname' => '127.0.0.1',
    // 数据库名
    'database' => 'mesfront',
    // 用户名
    'username' => 'root',
    // 密码
    'password' => 'root',
    // 端口
    'hostport' => '3306',
    // 数据库表前缀
    'prefix' => '',
    // 自动写入时间戳字段
    'auto_timestamp' => true,
];