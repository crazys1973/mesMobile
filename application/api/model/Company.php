<?php
/**
 * Created by 惠达浪
 * Email: crazys@126.com
 * Date: 2018/12/1
 * Time: 23:43
 */

namespace app\api\model;


use think\Model;

class Company extends Model {
    protected $hidden = ['create_time', 'update_time'];
}