<?php
/**
 * Created by 惠达浪
 * Email: crazys@126.com
 * Date: 2018/12/2
 * Time: 17:36
 */

namespace app\api\model;

use think\Model;

class Component extends Model {
    protected $hidden = ['create_time', 'update_time'];

}