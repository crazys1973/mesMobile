<?php
/**
 * Created by 惠达浪
 * Email: crazys@126.com
 * Date: 2018/12/5
 * Time: 11:30
 */

namespace app\api\model;

use think\Model;

class ResultProductReport extends Model {
    protected $type = [
        'rpr_starttime' => 'timestamp',
        'rpr_endtime' => 'timestamp',
        'rpr_quantity' => 'integer',
        'rpr_status' => 'integer'
    ];

    /**
     * 关联任务表result
     * @return \think\model\relation\BelongsTo|void
     */
    public function result() {
        return $this->belongsTo('Result', 'result_id', 'id');
    }

    /**
     * 关联用户表user
     * @return \think\model\relation\BelongsTo
     */
    public function user() {
        return $this->belongsTo('User', 'user_id', 'id');
    }
}