<?php
/**
 * Created by 惠达浪
 * Email: crazys@126.com
 * Date: 2018/11/20
 * Time: 22:55
 */

namespace app\api\facade;

use think\Facade;

/**
 * @see     \app\api\logic\User
 * @mixin \app\api\logic\User
 * @method array getUserInfoByToken(string $token) static 根据token获取用户信息
 * @method array login(string $username, string $password, string $device_no) static 用户登录，成功返回用户信息，失败返回错误码
 * @method bool workStart(string $username, string $password, int $workcenter_id) static 上班打卡
 * @method bool workEnd(string $username, string $password, int $workcenter_id) static 下班打卡
 * @method bool mobileLogin(string $username, string $password) static 手机端用户登录
 * @package app\api\facade
 */
class UserLogic extends Facade {
    /**
     * 获取当前Facade对应类名（或者已经绑定的容器对象标识）
     *
     * @access protected
     * @return string
     */
    protected static function getFacadeClass() {
        return 'app\api\logic\User';
    }
}