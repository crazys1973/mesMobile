<?php
/**
 * Created by 惠达浪
 * Email: crazys@126.com
 * Date: 2018/12/3
 * Time: 20:33
 */

namespace app\lib\enum;


class MesEnum {
    const ATTENDANCE_ON = 1; //登录设备
    const ATTENDANCE_OFF = 2; //退出设备
    const RESULT_NEW = 0; //任务未开始
    const RESULT_START = 1; //任务开始
    const RESULT_HAS_REPORTED = 2; //任务已报产
    const RESULT_FINISHED = 3; //任务完成
    const REPORT_UNFINISHED = 0; //报产未完成
    const REPORT_FINISHED = 1; //报产已完成
}