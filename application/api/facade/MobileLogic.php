<?php
/**
 * Created by PhpStorm.
 * User: crazy
 * Date: 2018/12/12
 * Time: 19:57
 */

namespace app\api\facade;

use think\Facade;

/**
 * @see     \app\api\logic\Mobile
 * @mixin \app\api\logic\Mobile
 * @method array getUserNewMessagesByToken(string $token) static 根据token获取用户站内信
 * @method \app\api\model\Article[] getNewArticles() static 获取最新消息
 * @method \app\api\model\Article[]|null getArticleById(int $id) static 获取指定分类的文章(分页)
 * @method \app\api\model\Article|null getArticlesByCategoryId(int $cid) static 获取指定id的文章
 * @method \app\api\model\Category getAllCategories() static 获取所有文章分类
 * @package app\api\facade
 */
class MobileLogic extends Facade {
    /**
     * 获取当前Facade对应类名（或者已经绑定的容器对象标识）
     *
     * @access protected
     * @return string
     */
    protected static function getFacadeClass() {
        return 'app\api\logic\Mobile';
    }
}