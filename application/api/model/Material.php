<?php
/**
 * Created by 惠达浪
 * Email: crazys@126.com
 * Date: 2018/12/2
 * Time: 15:34
 */

namespace app\api\model;

use think\Model;

class Material extends Model {
    protected $hidden = ['create_time', 'update_time'];

    /**
     * 关联stock表
     * 所属库房
     * @return \think\model\relation\BelongsTo
     */
    public function stock() {
        return $this->belongsTo('Stock', 'stock_id', 'id')->bind(['stk_name', 'stk_place']);
    }
}