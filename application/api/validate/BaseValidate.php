<?php
/**
 * Created by 惠达浪
 * Email: crazys@126.com
 * Date: 2018/11/22
 * Time: 14:59
 */

namespace app\api\validate;

use app\lib\exception\ParameterException;
use think\facade\Request;
use think\Validate;

/**
 * 验证类基类
 * @package app\api\validate
 */
class BaseValidate extends Validate {
    /**
     * 验证方法
     * 验证失败抛出API信息而非系统异常
     * @param null $params 待验证参数
     * @return bool 成功返回true，失败抛出异常
     * @throws ParameterException
     */
    public function goCheck($params = null) {
        if (is_null($params)) {
            $request = Request::instance();
            $params = $request->param();
        }

        if (!$this->check($params)) {
            //验证不通过，抛出参数错误异常
            throw new ParameterException($this->error);
        }
        return true;
    }
}