<?php
/**
 * Created by PhpStorm.
 * User: crazy
 * Date: 2018/12/12
 * Time: 16:49
 */

namespace app\api\model;

use think\Model;

class Banner extends Model {
    protected $hidden = ['create_time', 'update_time'];

    /**
     * 关联image表
     *
     * @return \think\model\relation\BelongsTo
     */
    public function image() {
        return $this->belongsTo('Image', 'image_id', 'id')->bind('img_url');
    }
}