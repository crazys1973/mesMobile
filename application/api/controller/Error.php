<?php
/**
 * Created by 惠达浪
 * Email: crazys@126.com
 * Date: 2018/11/27
 * Time: 23:20
 */

namespace app\index\controller;

use app\lib\enum\ErrorCodeEnum;

/**
 * 空控制器报错
 * @package app\index\controller
 */
class Error {
    public function index() {
        return json(['errorCode' => ErrorCodeEnum::UNKNOWN_ERROR, 'msg' => 'URL非法!'], 404);
    }

    public function _empty() {
        return json(['errorCode' => ErrorCodeEnum::UNKNOWN_ERROR, 'msg' => 'URL非法!'], 404);
    }
}