<?php
/**
 * Created by PhpStorm.
 * User: crazy
 * Date: 2018/12/7
 * Time: 21:06
 */

namespace app\api\model;

use think\Model;

class ClassModel extends Model {
    protected $table = 'class';
    protected $type = [
        'cls_begin' => 'timestamp',
        'cls_end' => 'timestamp'
    ];
}