<?php
/**
 * Created by 惠达浪
 * Email: crazys@126.com
 * Date: 2018/12/2
 * Time: 16:25
 */

namespace app\api\model;


use think\Model;

/**
 * mes工单部件模型
 * @package app\api\model
 */
class Mescomponent extends Model {
    protected $hidden = ['create_time', 'update_time'];

    /**
     * 关联component表
     * @return \think\model\relation\BelongsTo
     */
    public function component() {
        return $this->belongsTo('Component', 'component_id', 'id');
    }

    /**
     * 关联mesworksheet表
     * @return \think\model\relation\BelongsTo
     */
    public function mesworksheet() {
        return $this->belongsTo('Mesworksheet', 'mesworksheet_id', 'id');
    }

    /**
     * 关联mesprocess表
     * @return \think\model\relation\HasMany
     */
    public function mesprocess() {
        return $this->hasMany('Mesprocess', 'mescomponent_id', 'id');
    }

    /**
     * 关联mesmaterial表
     * @return \think\model\relation\HasMany
     */
    public function mesmaterial() {
        return $this->hasMany('Mesmaterial', 'mescomponent_id', 'id');
    }
}