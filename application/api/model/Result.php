<?php
/**
 * Created by 惠达浪
 * Email: crazys@126.com
 * Date: 2018/11/27
 * Time: 21:04
 */

namespace app\api\model;

use think\Model;

class Result extends Model {
    public $autoWriteTimestamp = false;
    protected $type = [
        'starttime_year_month' => 'timestamp',
        'endtime_year_month' => 'timestamp'
    ];

    /**
     * 关联mesworksheet工单主表
     * @return \think\model\relation\BelongsTo
     */
    public function mesworksheet() {
        return $this->belongsTo('Mesworksheet', 'planid', 'id');
    }

    /**
     * 关联mesprocess工单工序表
     * @return \think\model\relation\BelongsTo
     */
    public function mesprocess() {
        return $this->belongsTo('Mesprocess', 'processid', 'id');
    }

    /**
     * 关联workcenter工作中心
     * @return \think\model\relation\BelongsTo
     */
    public function workcenter() {
        return $this->belongsTo('Workcenter', 'workcenterid', 'id');
    }

    /**
     * 关联报产信息表result_product_repost
     * @return \think\model\relation\HasMany
     */
    public function report() {
        return $this->hasMany('ResultProductReport', 'result_id', 'id');
    }
}