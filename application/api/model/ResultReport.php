<?php
/**
 * Created by PhpStorm.
 * User: crazy
 * Date: 2018/12/10
 * Time: 16:31
 */

namespace app\api\model;


use think\Model;

class ResultReport extends Model {
    protected $updateTime = false;

    /**
     * 根据任务id及报道数量，返回生产总数
     *
     * @param int $result_id      任务id
     * @param int $reportQuantity 报产数量
     *
     * @return float
     */
    public function totalQuantity($result_id, $reportQuantity) {
        $sumQuantity = $this->where('result_id', '=', $result_id)->sum('quantity');
        return $sumQuantity + $reportQuantity;
    }
}