<?php
/**
 * Created by 惠达浪
 * Email: crazys@126.com
 * Date: 2018/11/29
 * Time: 8:32
 */

namespace app\lib\enum;


class LoginEnum {
    const USERNAME_ERROR = 0; //用户名不存在
    const PASSWORD_ERROR = 1; //密码错误
    const TEAM_ERROR = 2; //班组信息错误
    const CACHE_ERROR = 3; //设置缓存失败
}