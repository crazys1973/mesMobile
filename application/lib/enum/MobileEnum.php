<?php
/**
 * Created by PhpStorm.
 * User: crazy
 * Date: 2018/12/12
 * Time: 20:48
 */

namespace app\lib\enum;


class MobileEnum {
    const MESSAGE_STATUS_NEW = 0; //站内信未读状态
    const MESSAGE_STATUS_READ = 1; //站内信已读状态
    const CATEGORY_SHOW = 1; //导航上显示分类
    const CATEGORY_HIDDEN = 0; //导航不显示分类
}