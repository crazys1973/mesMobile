<?php
/**
 * Created by 惠达浪
 * Email: crazys@126.com
 * Date: 2018/11/27
 * Time: 23:20
 */

namespace app\index\controller;

use think\facade\Response;

/**
 * 空控制器报错
 * @package app\index\controller
 */
class Error {
    public function index() {
        return Response::create('非法访问！', null, 404);
    }

    public function _empty() {
        return Response::create('非法访问！', null, 404);
    }
}