<?php
/**
 * Created by PhpStorm.
 * User: crazy
 * Date: 2018/12/12
 * Time: 11:28
 */

namespace app\api\controller\v1;

use app\api\controller\BaseController;
use app\api\facade\MobileLogic;
use app\api\model\Banner;
use app\api\validate\MobileValidate;
use think\facade\Config;

class Mobile extends BaseController {
    /**
     * 获取轮播图
     *
     * @return \think\response
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function getBanner() {
        $bannerModel = new Banner();
        $slides_num = Config::get('global.slides_num') ?: 3;
        $slides = $bannerModel->order('create_time DESC')
            ->with('image')
            ->limit($slides_num)
            ->select();
        return $this->returnData($slides);
    }

    /**
     * 获取未读站内信
     *
     * @return \think\response
     */
    public function getNewMessage() {
        $this->checkToken();
        $messages = MobileLogic::getUserNewMessagesByToken($this->token);
        return $this->returnData($messages);
    }

    /**
     * 获取最新消息
     *
     * @return \think\response
     */
    public function getNews() {
        $articles = MobileLogic::getNewArticles();
        return $this->returnData($articles);
    }

    /**
     * 获取所有文章分类
     *
     * @return \think\response
     */
    public function getCategories() {
        $categories = MobileLogic::getAllCategories();
        return $this->returnData($categories);
    }

    public function getList() {
        (new MobileValidate())->scene('list')->goCheck();
        $cid = $this->request->get('category_id');
        $articles = MobileLogic::getArticlesByCategoryId($cid);
        return $this->returnData($articles);
    }

    /**
     * 获取指定id的文章
     *
     * @return \think\response
     * @throws \app\lib\exception\ParameterException
     */
    public function getArticle() {
        (new MobileValidate())->scene('article')->goCheck();
        $id = $this->request->get('id');
        $article = MobileLogic::getArticleById($id);
        return $this->returnData($article);
    }
}