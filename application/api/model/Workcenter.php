<?php
/**
 * Created by 惠达浪
 * Email: crazys@126.com
 * Date: 2018/11/27
 * Time: 21:06
 */

namespace app\api\model;

use think\Model;

class Workcenter extends Model {
    protected $hidden = ['create_time', 'update_time'];

    /**
     * 关联result表
     * @return \think\model\relation\HasMany
     */
    public function task() {
        return $this->hasMany('Result', 'workcenterid', 'id');
    }

    /**
     * 关联area表
     * @return \think\model\relation\BelongsTo
     */
    public function area() {
        return $this->belongsTo('Area', 'area_id', 'id');
    }
}