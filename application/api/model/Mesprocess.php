<?php
/**
 * Created by 惠达浪
 * Email: crazys@126.com
 * Date: 2018/11/27
 * Time: 21:27
 */

namespace app\api\model;


use think\Model;

class Mesprocess extends Model {
    protected $hidden = ['create_time', 'update_time'];

    /**
     * 关联process表
     * @return \think\model\relation\BelongsTo
     */
    public function process() {
        return $this->belongsTo('Process', 'process_id', 'id');
    }

    /**
     * 关联mesworksheet表
     * @return \think\model\relation\BelongsTo
     */
    public function mesworksheet() {
        return $this->belongsTo('Mesworksheet', 'mesworksheet_id', 'id');
    }

    /**
     * 关联mescomponent表
     * @return \think\model\relation\BelongsTo
     */
    public function mescomponent() {
        return $this->belongsTo('Mescomponent', 'mescomponent_id', 'id');
    }

    /**
     * 关联company表
     * @return \think\model\relation\BelongsTo
     */
    public function company() {
        return $this->belongsTo('Company', 'company_id', 'id');
    }

    /**
     * 关联mesprocess_sheet表
     * @return \think\model\relation\HasOne 返回印刷详情附表信息
     */
    public function detail() {
        return $this->hasOne('MesprocessSheet', 'mesprocess_id', 'id');
    }
}