﻿# Host: 127.0.0.1  (Version 5.5.53)
# Date: 2018-12-05 22:59:21
# Generator: MySQL-Front 6.1  (Build 1.26)


#
# Structure for table "admin"
#

DROP TABLE IF EXISTS `admin`;
CREATE TABLE `admin` (
  `Id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '用户id',
  `adm_username` varchar(30) NOT NULL DEFAULT '' COMMENT '管理员用户名',
  `adm_mobile` int(11) unsigned DEFAULT NULL COMMENT '手机号',
  `adm_password` varchar(100) DEFAULT NULL,
  `adm_salt` varchar(30) DEFAULT NULL COMMENT '加盐密钥',
  `adm_lastip` varchar(18) DEFAULT NULL COMMENT '最后登录IP',
  `adm_powerlist` varchar(500) DEFAULT NULL COMMENT '权限列表',
  `role_id` smallint(5) DEFAULT NULL COMMENT '角色id',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '添加时间',
  `update_time` int(11) unsigned DEFAULT NULL COMMENT '最后登录时间',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

#
# Data for table "admin"
#


#
# Structure for table "admin_role"
#

DROP TABLE IF EXISTS `admin_role`;
CREATE TABLE `admin_role` (
  `role_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT '角色id',
  `role_name` varchar(30) NOT NULL COMMENT '角色名称',
  `role_actlist` varchar(300) NOT NULL COMMENT '权限列表',
  `role_desc` varchar(30) NOT NULL COMMENT '角色描述',
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

#
# Data for table "admin_role"
#


#
# Structure for table "area"
#

DROP TABLE IF EXISTS `area`;
CREATE TABLE `area` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `are_name` varchar(30) DEFAULT NULL COMMENT '加工区域名称',
  `are_place` varchar(30) DEFAULT NULL COMMENT '加工区域位置',
  `company_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '隶属企业id',
  `are_memo` varchar(20) DEFAULT NULL COMMENT '备注',
  `create_time` int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) NOT NULL DEFAULT '0' COMMENT '更新时间',
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '创建人',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

#
# Data for table "area"
#


#
# Structure for table "article"
#

DROP TABLE IF EXISTS `article`;
CREATE TABLE `article` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `category_id` smallint(5) unsigned DEFAULT NULL COMMENT '类别ID',
  `art_title` varchar(150) DEFAULT NULL COMMENT '文章标题',
  `art_content` longtext COMMENT '文章内容',
  `art_author` varchar(30) DEFAULT NULL COMMENT '文章作者',
  `art_type` tinyint(3) NOT NULL DEFAULT '2' COMMENT '文章类型',
  `art_fileurl` varchar(255) DEFAULT NULL COMMENT '附件地址',
  `art_link` varchar(255) DEFAULT NULL COMMENT '链接地址',
  `art_description` mediumtext COMMENT '文章摘要',
  `art_click` int(11) NOT NULL DEFAULT '0' COMMENT '浏览量',
  `art_collect` int(11) NOT NULL DEFAULT '0' COMMENT '收藏数',
  `art_comment` int(11) NOT NULL DEFAULT '0' COMMENT '评论数',
  `art_zan` int(11) NOT NULL DEFAULT '0' COMMENT '点赞数',
  `art_publishtime` int(11) NOT NULL DEFAULT '0' COMMENT '文章发布时间',
  `art_thumb` varchar(255) DEFAULT NULL COMMENT '文章缩略图',
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '用户id',
  `art_top` tinyint(3) NOT NULL DEFAULT '0' COMMENT '文章置顶',
  `art_order` tinyint(3) NOT NULL DEFAULT '0' COMMENT '文章排序',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

#
# Data for table "article"
#


#
# Structure for table "attendance"
#

DROP TABLE IF EXISTS `attendance`;
CREATE TABLE `attendance` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `workcenter_id` int(11) NOT NULL COMMENT '登录的工作中心id',
  `user_id` int(11) NOT NULL COMMENT '登录的用户id',
  `attendance_time` int(11) NOT NULL COMMENT '登录/退出时间',
  `attendance_status` tinyint(4) NOT NULL COMMENT '登录/退出类型\n1、登录\n2、退出',
  PRIMARY KEY (`id`),
  UNIQUE KEY `attendance_id_uindex` (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COMMENT='上机出勤信息表';

#
# Data for table "attendance"
#

/*!40000 ALTER TABLE `attendance` DISABLE KEYS */;
INSERT INTO `attendance` VALUES (1,1,1,1543843502,1),(2,1,1,1543848960,1),(3,1,1,1543850251,2),(4,1,1,1543989653,2);
/*!40000 ALTER TABLE `attendance` ENABLE KEYS */;

#
# Structure for table "banner"
#

DROP TABLE IF EXISTS `banner`;
CREATE TABLE `banner` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `bnr_kind` varchar(10) NOT NULL DEFAULT '0' COMMENT 'BANNER类别',
  `bnr_name` varchar(10) NOT NULL DEFAULT '0' COMMENT 'banner名称',
  `bnr_desc` varchar(10) NOT NULL DEFAULT '0' COMMENT 'banner描述',
  `image_id` int(11) NOT NULL DEFAULT '0' COMMENT '图片id',
  `bnr_redirect` varchar(200) NOT NULL DEFAULT '0' COMMENT '图片跳转到',
  `create_time` int(11) DEFAULT NULL COMMENT '添加时间',
  `update_time` int(11) DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

#
# Data for table "banner"
#


#
# Structure for table "category"
#

DROP TABLE IF EXISTS `category`;
CREATE TABLE `category` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `cat_name` varchar(20) DEFAULT NULL COMMENT '类别名称',
  `cat_type` smallint(5) NOT NULL DEFAULT '0' COMMENT '系统分组',
  `parent_id` smallint(5) DEFAULT NULL COMMENT '父级ID',
  `cat_show` tinyint(3) NOT NULL DEFAULT '0' COMMENT '是否导航显示',
  `cat_order` smallint(5) NOT NULL DEFAULT '0' COMMENT '排序',
  `cat_desc` varchar(255) DEFAULT NULL COMMENT '分类描述',
  `cat_keywords` varchar(30) DEFAULT NULL COMMENT '搜索关键词',
  `cat_alias` varchar(20) DEFAULT NULL COMMENT '别名',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

#
# Data for table "category"
#


#
# Structure for table "class"
#

DROP TABLE IF EXISTS `class`;
CREATE TABLE `class` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `cls_name` varchar(10) NOT NULL DEFAULT '0' COMMENT '班次名称',
  `cls_begin` varchar(10) NOT NULL DEFAULT '0' COMMENT '开始时间',
  `cls_end` varchar(10) NOT NULL DEFAULT '0' COMMENT '结束时间',
  `workcenter_id` int(11) NOT NULL DEFAULT '0' COMMENT '绑定工作中心',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

#
# Data for table "class"
#


#
# Structure for table "company"
#

DROP TABLE IF EXISTS `company`;
CREATE TABLE `company` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `co_creditno` varchar(20) DEFAULT NULL COMMENT '信用代码',
  `co_companyname` varchar(20) DEFAULT NULL COMMENT '企业名称',
  `co_kind` tinyint(3) DEFAULT NULL COMMENT '企业类型',
  `co_industry` varchar(6) DEFAULT NULL COMMENT '隶属行业',
  `co_address` varchar(30) DEFAULT NULL COMMENT '企业地址',
  `co_desc` varchar(100) DEFAULT NULL COMMENT '企业介绍',
  `co_tel` varchar(20) DEFAULT NULL COMMENT '联系电话',
  `co_email` varchar(20) DEFAULT NULL COMMENT '电子邮箱',
  `co_map` varchar(20) DEFAULT NULL COMMENT '地图定位',
  `co_audit` int(11) NOT NULL DEFAULT '0' COMMENT '审核状态',
  `create_time` int(11) NOT NULL DEFAULT '0' COMMENT '注册时间 时间戳',
  `update_time` int(11) NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

#
# Data for table "company"
#

INSERT INTO `company` VALUES (1,NULL,'真正的企业名称',NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0);

#
# Structure for table "company_equipment"
#

DROP TABLE IF EXISTS `company_equipment`;
CREATE TABLE `company_equipment` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `company_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '企业id',
  `equipment_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '设备id',
  `workcenter_id` int(11) DEFAULT NULL COMMENT '工作中心id',
  `equip_aliases` varchar(20) DEFAULT NULL COMMENT '设备别名',
  `create_time` int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) NOT NULL DEFAULT '0' COMMENT '更新时间',
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '创建人',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

#
# Data for table "company_equipment"
#


#
# Structure for table "component"
#

DROP TABLE IF EXISTS `component`;
CREATE TABLE `component` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `cpn_id` varchar(10) NOT NULL DEFAULT '0' COMMENT '部件编码',
  `cpn_name` varchar(30) DEFAULT NULL COMMENT '部件名称',
  `create_time` int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) NOT NULL DEFAULT '0' COMMENT '更新时间',
  `cpn_memo` varchar(10) DEFAULT NULL COMMENT '备注',
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '创建人',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

#
# Data for table "component"
#

INSERT INTO `component` VALUES (1,'8859','部件名称',0,0,NULL,1);

#
# Structure for table "demand"
#

DROP TABLE IF EXISTS `demand`;
CREATE TABLE `demand` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '编号',
  `dmd_kind` tinyint(3) DEFAULT NULL COMMENT '产品分类',
  `dmd_address` varchar(20) DEFAULT NULL COMMENT '所在地点',
  `dmd_name` varchar(20) DEFAULT NULL COMMENT '产品名称',
  `dmd_price` int(11) DEFAULT NULL COMMENT '价格',
  `dmd_count` smallint(11) DEFAULT NULL COMMENT '库存数量',
  `dmd_images` varchar(30) DEFAULT NULL COMMENT '相册',
  `dmd_timelimit` int(11) DEFAULT NULL COMMENT '送达交付期限',
  `dmd_contact` tinyint(3) DEFAULT NULL COMMENT '联系方式',
  `dmd_desc` varchar(500) DEFAULT NULL COMMENT '详情介绍',
  `dmd_user_id` int(11) DEFAULT NULL COMMENT '发布人id',
  `dmd_status` tinyint(3) DEFAULT NULL COMMENT '发布状态',
  `dmd_audit` tinyint(3) DEFAULT NULL COMMENT '审核',
  `create_time` int(11) DEFAULT NULL COMMENT '注册时间 时间戳',
  `update_time` int(11) DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

#
# Data for table "demand"
#


#
# Structure for table "equipment"
#

DROP TABLE IF EXISTS `equipment`;
CREATE TABLE `equipment` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `equ_kind` varchar(20) NOT NULL DEFAULT '0' COMMENT '设备类名称',
  `equ_name` varchar(30) NOT NULL DEFAULT '0' COMMENT '设备名称',
  `equ_factory` varchar(30) DEFAULT NULL COMMENT '厂商',
  `equ_model` varchar(30) DEFAULT NULL COMMENT '型号',
  `equ_buydate` int(11) DEFAULT NULL COMMENT '购买日期',
  `equ_price` int(11) DEFAULT NULL COMMENT '购买价格',
  `equ_status` varchar(10) DEFAULT NULL COMMENT '设备状态',
  `equ_capacity` int(11) DEFAULT NULL COMMENT '标准产能值',
  `equ_unit` varchar(5) DEFAULT NULL COMMENT '标准产能单位',
  `equ_operate_min` int(11) DEFAULT NULL COMMENT '最小上机',
  `equ_operate_max` int(11) DEFAULT NULL COMMENT '最大上机',
  `equ_color_max` tinyint(3) DEFAULT NULL COMMENT '最大印色',
  `equ_size_min` int(11) DEFAULT NULL COMMENT '体积最小值',
  `equ_size_max` int(11) DEFAULT NULL COMMENT '体积最大值',
  `equ_mass_min` int(11) DEFAULT NULL COMMENT '质量最小值',
  `equ_mass_max` int(11) DEFAULT NULL COMMENT '质量最大值',
  `equ_temp_min` int(11) DEFAULT NULL COMMENT '温度最小值',
  `equ_temp_max` int(11) DEFAULT NULL COMMENT '温度最大值',
  `equ_wet_min` tinyint(3) DEFAULT NULL COMMENT '湿度最小值',
  `equ_wet_max` tinyint(3) DEFAULT NULL COMMENT '湿度最大值',
  `equ_light_min` int(11) DEFAULT NULL COMMENT '光照最小值',
  `equ_light_max` int(11) DEFAULT NULL COMMENT '光照最大值',
  `equ_stop_min` int(11) DEFAULT NULL COMMENT '固有停顿时间min',
  `equ_stop_max` int(11) DEFAULT NULL COMMENT '固有停顿时间max',
  `equ_memo` varchar(20) DEFAULT NULL COMMENT '备注',
  `workcenter_id` int(11) DEFAULT NULL COMMENT '工作中心id',
  `create_time` int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) NOT NULL DEFAULT '0' COMMENT '更新时间',
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '创建人',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

#
# Data for table "equipment"
#


#
# Structure for table "equipment_sort"
#

DROP TABLE IF EXISTS `equipment_sort`;
CREATE TABLE `equipment_sort` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `eqs_name` varchar(30) NOT NULL DEFAULT '0' COMMENT '设备类名称',
  `create_time` int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `eqs_memo` varchar(20) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

#
# Data for table "equipment_sort"
#


#
# Structure for table "erp_gx"
#

DROP TABLE IF EXISTS `erp_gx`;
CREATE TABLE `erp_gx` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `djbh` varchar(20) DEFAULT NULL COMMENT '工单号',
  `cpbj` varchar(20) DEFAULT NULL COMMENT '产品部件',
  `gxbm` varchar(20) DEFAULT NULL COMMENT '工序',
  `dhs` varchar(20) DEFAULT NULL COMMENT '工艺损耗数',
  `wxbz` varchar(20) DEFAULT NULL COMMENT '外协',
  `gyyq` varchar(20) DEFAULT NULL COMMENT '工艺要求',
  `sl` varchar(20) DEFAULT NULL COMMENT '需加工数量',
  `wcsl` varchar(20) DEFAULT NULL COMMENT '完工数量',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

#
# Data for table "erp_gx"
#


#
# Structure for table "erp_main"
#

DROP TABLE IF EXISTS `erp_main`;
CREATE TABLE `erp_main` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `djbh` varchar(20) DEFAULT NULL COMMENT '工单号',
  `rwlx` varchar(10) DEFAULT NULL COMMENT '工单类型（*）',
  `cpmc` varchar(20) DEFAULT NULL COMMENT '产品名称',
  `cpgg` varchar(20) DEFAULT NULL COMMENT '成品尺寸',
  `mc` varchar(20) DEFAULT NULL COMMENT '客户名称',
  `cdjbh` varchar(20) DEFAULT NULL COMMENT '合同号',
  `htsl` int(11) DEFAULT NULL COMMENT '合同数量',
  `jhfs` varchar(20) DEFAULT NULL COMMENT '交货方式',
  `rq` int(11) DEFAULT NULL COMMENT '下单日期',
  `jhrq` int(11) DEFAULT NULL COMMENT '交货日期',
  `zdr` varchar(10) DEFAULT NULL COMMENT '制单人（*）',
  `qybm` varchar(10) DEFAULT NULL COMMENT '业务员（*）',
  `bz` varchar(10) DEFAULT NULL COMMENT '备注',
  `create_time` int(11) DEFAULT NULL COMMENT '创建时间（*）',
  `image` varchar(30) DEFAULT NULL COMMENT 'ERP工单生成图片的URL',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

#
# Data for table "erp_main"
#


#
# Structure for table "erp_wl"
#

DROP TABLE IF EXISTS `erp_wl`;
CREATE TABLE `erp_wl` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `djbh` varchar(20) DEFAULT NULL COMMENT '工单号',
  `cpbj` varchar(20) DEFAULT NULL COMMENT '部件',
  `mc` varchar(20) DEFAULT NULL COMMENT '物料名称',
  `ckbm` varchar(20) DEFAULT NULL COMMENT '物料来源',
  `gg` varchar(20) DEFAULT NULL COMMENT '用料规格',
  `sl` varchar(20) DEFAULT NULL COMMENT '用料数量',
  `jldw` varchar(20) DEFAULT NULL COMMENT '单位',
  `cc` varchar(20) DEFAULT NULL COMMENT '开料尺寸',
  `ys` varchar(20) DEFAULT NULL COMMENT '开料数量',
  `ks` varchar(20) DEFAULT NULL COMMENT '开数',
  `bz` varchar(20) DEFAULT NULL COMMENT '余料处理',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

#
# Data for table "erp_wl"
#


#
# Structure for table "erp_ys"
#

DROP TABLE IF EXISTS `erp_ys`;
CREATE TABLE `erp_ys` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `djbh` varchar(20) DEFAULT NULL COMMENT '工单号',
  `cpbj` varchar(20) DEFAULT NULL COMMENT '产品部件',
  `yzs` varchar(10) DEFAULT NULL COMMENT '印张正数',
  `cc` varchar(10) DEFAULT NULL COMMENT '印张尺寸',
  `bzsl` varchar(10) DEFAULT NULL COMMENT '套数',
  `ys` varchar(10) DEFAULT NULL COMMENT '色数',
  `zzs` varchar(10) DEFAULT NULL COMMENT '正面专色',
  `fzs` varchar(10) DEFAULT NULL COMMENT '反面专色',
  `sbfs` varchar(10) DEFAULT NULL COMMENT '晒版方式',
  `fsl` varchar(10) DEFAULT NULL COMMENT '用版数（*）',
  `fs` varchar(10) DEFAULT NULL COMMENT '印刷方式',
  `bz` varchar(10) DEFAULT NULL COMMENT '印刷要求',
  `create_time` int(11) DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

#
# Data for table "erp_ys"
#


#
# Structure for table "image"
#

DROP TABLE IF EXISTS `image`;
CREATE TABLE `image` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `img_url` varchar(200) NOT NULL DEFAULT '0' COMMENT '图片URL',
  `img_from` tinyint(3) NOT NULL DEFAULT '1' COMMENT '图片来自1本地2公网',
  `create_time` int(11) DEFAULT NULL COMMENT '添加时间',
  `update_time` int(11) DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

#
# Data for table "image"
#


#
# Structure for table "log"
#

DROP TABLE IF EXISTS `log`;
CREATE TABLE `log` (
  `Id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `log_kind` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '日志类别',
  `log_userid` int(11) NOT NULL DEFAULT '0' COMMENT '操作人userid',
  `log_url` varchar(100) NOT NULL DEFAULT '0' COMMENT '日志错误的URL',
  `log_ip` varchar(18) NOT NULL DEFAULT '0' COMMENT '报错的IP地址',
  `log_desc` varchar(100) NOT NULL DEFAULT '0' COMMENT '日志详情',
  `create_time` int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

#
# Data for table "log"
#


#
# Structure for table "material"
#

DROP TABLE IF EXISTS `material`;
CREATE TABLE `material` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `meterialsort_id` int(11) NOT NULL DEFAULT '0' COMMENT '物料类编号',
  `stock_id` int(11) NOT NULL DEFAULT '0' COMMENT '备料区编号',
  `mtl_id` int(11) NOT NULL DEFAULT '0' COMMENT '物料编号',
  `mtl_name` varchar(20) DEFAULT NULL COMMENT '物料名称',
  `mtl_weight` int(11) DEFAULT NULL COMMENT '克重',
  `mtl_size` varchar(10) DEFAULT NULL COMMENT '规格',
  `mtl_unit` varchar(6) DEFAULT NULL COMMENT '单位',
  `mtl_memo` varchar(20) DEFAULT NULL COMMENT '备注',
  `create_time` int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) NOT NULL DEFAULT '0' COMMENT '更新时间',
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '创建人',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

#
# Data for table "material"
#

INSERT INTO `material` VALUES (1,1,1,3129989,'物料名称',23,'规格','单位',NULL,0,0,1),(2,1,1,8289844,'另一个物料',25,'也是规格','单位',NULL,0,0,1);

#
# Structure for table "materialsort"
#

DROP TABLE IF EXISTS `materialsort`;
CREATE TABLE `materialsort` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `mts_name` varchar(20) DEFAULT NULL COMMENT '物料类名称',
  `mts_memo` varchar(20) DEFAULT NULL COMMENT '备注',
  `create_time` int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) NOT NULL DEFAULT '0' COMMENT '更新时间',
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '创建人',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

#
# Data for table "materialsort"
#


#
# Structure for table "mescomponent"
#

DROP TABLE IF EXISTS `mescomponent`;
CREATE TABLE `mescomponent` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '部件编号',
  `mesworksheet_id` int(11) DEFAULT NULL COMMENT 'mes工单号',
  `component_id` int(11) DEFAULT NULL COMMENT '基础部件编号',
  `memo` varchar(20) DEFAULT NULL COMMENT '备注',
  `create_time` int(11) DEFAULT NULL COMMENT '创建时间',
  `update_time` int(11) DEFAULT NULL COMMENT '更新时间',
  `user_id` int(11) DEFAULT NULL COMMENT '创建人',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

#
# Data for table "mescomponent"
#

INSERT INTO `mescomponent` VALUES (1,1,1,NULL,NULL,NULL,1);

#
# Structure for table "mesdoubleprocess"
#

DROP TABLE IF EXISTS `mesdoubleprocess`;
CREATE TABLE `mesdoubleprocess` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `mesprocess_id` int(11) DEFAULT NULL COMMENT '本工序id',
  `mesprocess_nextid` int(11) DEFAULT NULL COMMENT '下工序id',
  `mescomponent_id` int(11) DEFAULT NULL COMMENT 'mes部件id',
  `mdp_interval_min` int(11) DEFAULT NULL COMMENT '工序间隔 分钟min',
  `mdp_interval_max` int(11) DEFAULT NULL COMMENT '工序间隔 分钟max',
  `mdp_workcenter` varchar(20) DEFAULT NULL COMMENT '可用工作中心 多个',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

#
# Data for table "mesdoubleprocess"
#


#
# Structure for table "mesmaterial"
#

DROP TABLE IF EXISTS `mesmaterial`;
CREATE TABLE `mesmaterial` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '部件编号',
  `mesworksheet_id` int(11) DEFAULT NULL COMMENT 'mes工单号',
  `mescomponent_id` int(11) DEFAULT NULL COMMENT 'Mes部件编号',
  `material_id` int(11) DEFAULT NULL COMMENT '物料编号',
  `mmt_suite` int(11) DEFAULT NULL COMMENT '套数',
  `mmt_quantity` int(11) DEFAULT NULL COMMENT '可用量',
  `mmt_memo` varchar(20) DEFAULT NULL COMMENT '备注',
  `create_time` int(11) DEFAULT NULL COMMENT '创建时间',
  `update_time` int(11) DEFAULT NULL COMMENT '更新时间',
  `user_id` int(11) DEFAULT NULL COMMENT '创建人',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

#
# Data for table "mesmaterial"
#

INSERT INTO `mesmaterial` VALUES (1,1,1,1,NULL,NULL,NULL,NULL,NULL,1),(2,1,1,2,NULL,NULL,NULL,NULL,NULL,1);

#
# Structure for table "mesprocess"
#

DROP TABLE IF EXISTS `mesprocess`;
CREATE TABLE `mesprocess` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '部件编号',
  `process_id` int(11) DEFAULT NULL COMMENT '基础工序编号',
  `mesworksheet _id` int(11) DEFAULT NULL COMMENT 'mes工单号',
  `mescomponent_id` int(11) DEFAULT NULL COMMENT 'mes部件编号',
  `msp_outsourcing` int(11) DEFAULT NULL COMMENT '外协企业id',
  `msp_demand` varchar(30) DEFAULT NULL COMMENT '工艺要求',
  `msp_quantity` int(11) DEFAULT NULL COMMENT '需加工数量',
  `msp_nextid` int(11) DEFAULT NULL COMMENT '下工序编码',
  `msp_interval` int(11) DEFAULT NULL COMMENT '间隔时间（分钟）',
  `msp_suite` int(11) DEFAULT NULL COMMENT '套数（*）',
  `msp_workcenter` varchar(30) DEFAULT NULL COMMENT '可用工作中心（多个）',
  `company_id` int(11) DEFAULT NULL COMMENT '隶属企业',
  `msp_memo` varchar(10) DEFAULT NULL COMMENT '备注',
  `user_id` int(11) DEFAULT NULL COMMENT '创建人',
  `create_time` int(11) DEFAULT NULL COMMENT '创建时间',
  `update_time` int(11) DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

#
# Data for table "mesprocess"
#

INSERT INTO `mesprocess` VALUES (1,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,NULL),(2,2,1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,NULL);

#
# Structure for table "mesprocess_sheet"
#

DROP TABLE IF EXISTS `mesprocess_sheet`;
CREATE TABLE `mesprocess_sheet` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `mesprocess_id` int(11) DEFAULT NULL COMMENT 'mes工单工序Id',
  `mps_yzs` varchar(10) DEFAULT NULL COMMENT '印张正数',
  `mps_cc` varchar(10) DEFAULT NULL COMMENT '印张尺寸',
  `mps_bzsl` varchar(10) DEFAULT NULL COMMENT '套数',
  `mps_ys` varchar(10) DEFAULT NULL COMMENT '色数',
  `mps_zzs` varchar(10) DEFAULT NULL COMMENT '正面专色',
  `mps_fzs` varchar(10) DEFAULT NULL COMMENT '反面专色',
  `mps_sbfs` varchar(10) DEFAULT NULL COMMENT '晒版方式',
  `mps_fsl` varchar(10) DEFAULT NULL COMMENT '用版数（*）',
  `mps_fs` varchar(10) DEFAULT NULL COMMENT '印刷方式',
  `mps_bz` varchar(10) DEFAULT NULL COMMENT '印刷要求',
  `user_id` int(11) DEFAULT NULL COMMENT '创建人',
  `create_time` int(11) DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

#
# Data for table "mesprocess_sheet"
#

INSERT INTO `mesprocess_sheet` VALUES (1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(2,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);

#
# Structure for table "mesworksheet"
#

DROP TABLE IF EXISTS `mesworksheet`;
CREATE TABLE `mesworksheet` (
  `id` bigint(15) unsigned NOT NULL AUTO_INCREMENT COMMENT 'mes工单号编号',
  `mws_priority` tinyint(3) DEFAULT NULL COMMENT '优先级（待计算）',
  `mws_deliverydate` int(11) DEFAULT NULL COMMENT '交付期',
  `mws_productsize` varchar(20) DEFAULT NULL COMMENT '成品尺寸',
  `mws_company` int(11) DEFAULT NULL COMMENT '隶属企业',
  `mws_orderno` varchar(20) DEFAULT NULL COMMENT 'ERP工单号',
  `mws_customer` varchar(30) DEFAULT NULL COMMENT '客户名称',
  `mws_customerid` int(11) DEFAULT NULL COMMENT '客户id',
  `mws_product` varchar(20) DEFAULT NULL COMMENT '订单产品名称',
  `mws_orderdate` int(11) DEFAULT NULL COMMENT '下单日期',
  `mws_amount` int(11) DEFAULT NULL COMMENT '交货数量',
  `mws_memo` varchar(20) DEFAULT NULL COMMENT '备注',
  `create_time` int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) NOT NULL DEFAULT '0' COMMENT '更新时间',
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '创建人',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

#
# Data for table "mesworksheet"
#

INSERT INTO `mesworksheet` VALUES (1,NULL,NULL,NULL,1,'订单号',NULL,NULL,NULL,NULL,NULL,NULL,0,0,1);

#
# Structure for table "process"
#

DROP TABLE IF EXISTS `process`;
CREATE TABLE `process` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `prc_id` varchar(15) NOT NULL DEFAULT '0' COMMENT '工序编号',
  `prc_name` varchar(30) DEFAULT NULL COMMENT '工序名称',
  `prc_sort` varchar(30) DEFAULT NULL COMMENT '工序类别',
  `prc_desc` varchar(50) DEFAULT NULL COMMENT '工序描述',
  `prc_outsourcing` tinyint(3) NOT NULL DEFAULT '0' COMMENT '外协工序',
  `company_id` int(11) NOT NULL DEFAULT '0' COMMENT '隶属企业',
  `create_time` int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) NOT NULL DEFAULT '0' COMMENT '更新时间',
  `prc_memo` varchar(10) DEFAULT NULL COMMENT '备注',
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '创建人',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

#
# Data for table "process"
#


#
# Structure for table "process_workcenter"
#

DROP TABLE IF EXISTS `process_workcenter`;
CREATE TABLE `process_workcenter` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `process_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '工序id',
  `workcenter_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '工作中心id',
  `create_time` int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

#
# Data for table "process_workcenter"
#


#
# Structure for table "result"
#

DROP TABLE IF EXISTS `result`;
CREATE TABLE `result` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `planid` int(11) NOT NULL DEFAULT '0' COMMENT '母订单编号 MES工单号',
  `processid` int(11) NOT NULL DEFAULT '0' COMMENT 'mes工序编号',
  `workcenterid` int(11) NOT NULL DEFAULT '0' COMMENT '工作中心编号',
  `starttime_year_month` int(11) NOT NULL DEFAULT '0' COMMENT '即工序开始时间的年月日',
  `endtime_year_month` int(11) NOT NULL DEFAULT '0' COMMENT '即工序开结束间的年月日',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

#
# Data for table "result"
#

INSERT INTO `result` VALUES (1,1,1,1,1543326941,0);

#
# Structure for table "result_product_report"
#

DROP TABLE IF EXISTS `result_product_report`;
CREATE TABLE `result_product_report` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `result_id` int(11) NOT NULL,
  `rpr_starttime` int(11) NOT NULL DEFAULT '0',
  `rpr_endtime` int(11) NOT NULL DEFAULT '0',
  `rpr_quantity` int(11) NOT NULL DEFAULT '0',
  `rpr_status` tinyint(4) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `result_product_report_id_uindex` (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COMMENT='报产信息表';

#
# Data for table "result_product_report"
#

/*!40000 ALTER TABLE `result_product_report` DISABLE KEYS */;
INSERT INTO `result_product_report` VALUES (4,0,1543969800,1543986960,500,0,1),(5,0,1543969800,1543986960,500,0,1),(6,0,1543969800,1543986960,500,0,1),(7,0,1543969800,1543986960,500,0,1);
/*!40000 ALTER TABLE `result_product_report` ENABLE KEYS */;

#
# Structure for table "stock"
#

DROP TABLE IF EXISTS `stock`;
CREATE TABLE `stock` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `stk_name` varchar(20) DEFAULT NULL COMMENT '备料区名称',
  `stk_place` varchar(20) DEFAULT NULL COMMENT '所在位置',
  `company_id` int(11) NOT NULL DEFAULT '0' COMMENT '隶属企业',
  `create_time` int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) NOT NULL DEFAULT '0' COMMENT '更新时间',
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '创建人',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

#
# Data for table "stock"
#

INSERT INTO `stock` VALUES (1,'公司库房',NULL,1,0,0,1);

#
# Structure for table "team"
#

DROP TABLE IF EXISTS `team`;
CREATE TABLE `team` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '班组id',
  `tem_name` varchar(20) NOT NULL DEFAULT '' COMMENT '班组名称 ',
  `tem_desc` varchar(20) DEFAULT NULL COMMENT '班组描述',
  `workcenter_id` int(11) unsigned DEFAULT NULL COMMENT '绑定工作中心',
  `tem_leader` int(11) DEFAULT NULL COMMENT '班组长id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

#
# Data for table "team"
#

INSERT INTO `team` VALUES (1,'测试班组','就是用来测试的，所以要怎么描述？',NULL,1),(2,'2号班组',NULL,NULL,2);

#
# Structure for table "user"
#

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '用户id',
  `usr_mobile` char(11) NOT NULL COMMENT '用户手机号码/用户名',
  `usr_pwd` varchar(32) NOT NULL COMMENT '登录密码',
  `usr_kind` tinyint(3) NOT NULL DEFAULT '0' COMMENT '用户类别',
  `company_id` int(11) NOT NULL DEFAULT '0' COMMENT '隶属企业',
  `usr_leader` tinyint(11) DEFAULT '0' COMMENT '是否负责人',
  `usr_lastlogin` int(11) DEFAULT NULL COMMENT '最后一次登录时间',
  `usr_lastip` varchar(18) DEFAULT NULL COMMENT '最后一次登录ip',
  `usr_openid` varchar(30) DEFAULT NULL COMMENT 'opened 备用',
  `usr_unionid` varchar(30) DEFAULT NULL COMMENT 'unionid备用',
  `usr_token` varchar(100) DEFAULT NULL COMMENT '验证token',
  `usr_timeout` int(11) DEFAULT NULL COMMENT 'token有效期',
  `usr_nickname` varchar(10) DEFAULT NULL COMMENT '昵称',
  `usr_headpic` varchar(60) DEFAULT NULL COMMENT '头像图片',
  `usr_signature` varchar(100) DEFAULT NULL COMMENT '个性签名',
  `usr_sex` tinyint(3) NOT NULL DEFAULT '0' COMMENT '1男0女',
  `usr_memo` varchar(20) DEFAULT NULL COMMENT '备注',
  `usr_audit` tinyint(3) DEFAULT '0' COMMENT '审核状态',
  `create_time` int(11) DEFAULT NULL COMMENT '注册时间 时间戳',
  `update_time` int(11) DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

#
# Data for table "user"
#

INSERT INTO `user` VALUES (1,'18645978448','190bcc4a7d7a37fa164ac706cdfa313a',0,0,0,1543659529,'127.0.0.1',NULL,NULL,'user5c026009a4f33',NULL,'灰大狼',NULL,NULL,0,NULL,0,NULL,NULL);

#
# Structure for table "user_team"
#

DROP TABLE IF EXISTS `user_team`;
CREATE TABLE `user_team` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `user_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '用户id ',
  `team_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '班组id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

#
# Data for table "user_team"
#

INSERT INTO `user_team` VALUES (1,1,1),(2,1,2);

#
# Structure for table "userinfo"
#

DROP TABLE IF EXISTS `userinfo`;
CREATE TABLE `userinfo` (
  `user_id` int(11) NOT NULL COMMENT '用户id',
  `uif_realname` varchar(20) NOT NULL COMMENT '姓名',
  `uif_sex` tinyint(3) NOT NULL COMMENT '性别',
  `uif_birthday` int(11) DEFAULT NULL COMMENT '出生日期',
  `uif_address` varchar(100) DEFAULT NULL COMMENT '现居住地',
  `uif_area` varchar(50) DEFAULT NULL COMMENT '区域',
  `uif_idcard` varchar(20) DEFAULT NULL COMMENT '身份证号码',
  `uif_idcard_face` varchar(100) DEFAULT NULL COMMENT '身份证正面照片',
  `uif_idcard_back` varchar(100) DEFAULT NULL COMMENT '身份证反面照片',
  `uif_phone` varchar(11) DEFAULT NULL COMMENT '手机',
  `uif_email` varchar(20) DEFAULT NULL COMMENT '邮箱',
  `uif_auth_status` tinyint(3) DEFAULT NULL COMMENT '验证',
  `uif_memo` varchar(20) DEFAULT NULL COMMENT '备注',
  `create_time` int(11) NOT NULL COMMENT '注册时间 时间戳',
  `update_time` int(11) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

#
# Data for table "userinfo"
#


#
# Structure for table "workcenter"
#

DROP TABLE IF EXISTS `workcenter`;
CREATE TABLE `workcenter` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `area_id` int(11) NOT NULL DEFAULT '0' COMMENT '加工区域id',
  `wkc_name` varchar(30) DEFAULT NULL COMMENT '工作中心名称',
  `company_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '隶属企业id',
  `wkc_outsourcing` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '外协工作中心id',
  `wkc_memo` varchar(20) DEFAULT NULL COMMENT '备注',
  `create_time` int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) NOT NULL DEFAULT '0' COMMENT '更新时间',
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '创建人',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COMMENT='工作中心表';

#
# Data for table "workcenter"
#

INSERT INTO `workcenter` VALUES (1,1,'中心名称',1,0,NULL,0,0,1);
