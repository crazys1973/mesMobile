<?php
/**
 * Created by 惠达浪
 * Email: crazys@126.com
 * Date: 2018/11/22
 * Time: 15:03
 */

namespace app\lib\exception;

use app\lib\enum\ErrorCodeEnum;

/**
 * 参数错误异常
 * @package app\lib\exception
 */
class ParameterException extends BaseException {
    public function __construct($message) {
        //参数错误，强行将状态码设置为400
        parent::__construct(400, $message, ErrorCodeEnum::PARAMETER_ERROR);
    }
}