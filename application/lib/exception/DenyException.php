<?php
/**
 * Created by PhpStorm.
 * User: crazy
 * Date: 2018/12/11
 * Time: 17:31
 */

namespace app\lib\exception;

class DenyException extends BaseException {
    public function __construct($message = null, $errorCode = null) {
        parent::__construct(403, $message, $errorCode);
    }
}