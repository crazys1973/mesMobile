<?php
/**
 * Created by 惠达浪
 * Email: crazys@126.com
 * Date: 2018/11/22
 * Time: 15:15
 */

namespace app\api\model;

use think\facade\Request;
use think\Model;

class User extends Model {
    protected $readonly = ['id'];
    protected $hidden = ['usr_pwd', 'usr_token'];
    protected $update = ['usr_lastip'];
    protected $type = ['usr_lastlogin' => 'timestamp'];

    public function setUsrLastipAttr() {
        return Request::ip();
    }

    /**
     * 所属班组
     * @return \think\model\relation\BelongsToMany
     */
    public function team() {
        return $this->belongsToMany('Team', 'user_team', 'team_id', 'user_id');
    }

}