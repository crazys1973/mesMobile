<?php
/**
 * Created by 惠达浪
 * Email: crazys@126.com
 * Date: 2018/11/23
 * Time: 0:12
 */

namespace app\api\validate;


class UserValidate extends BaseValidate {
    protected $rule = [
        'username|用户名' => 'require',
        'password|密码' => 'require',
        'device_no|工作中心设备号' => 'require',
        'result_id|任务id' => 'require|number'
    ];

    protected $message = [

    ];

    protected $scene = [
        'login' => ['username', 'password', 'device_no'],
        'work' => ['username', 'password', 'result_id'],
        'mobile_login' => ['username', 'password']
    ];
}