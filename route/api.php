<?php
/**
 * Created by 惠达浪
 * Email: crazys@126.com
 * Date: 2018/11/21
 * Time: 0:43
 */

Route::controller('api/:version/user','api/:version.User')->allowCrossDomain();
Route::controller('api/:version/mes','api/:version.Mes')->allowCrossDomain();
Route::controller('api/:version/mobile','api/:version.Mobile')->allowCrossDomain();