<?php
/**
 * Created by 惠达浪
 * Email: crazys@126.com
 * Date: 2018/11/22
 * Time: 23:18
 */

namespace app\lib\enum;

/**
 * 全局错误码
 *
 * @package app\lib\enum
 */
class ErrorCodeEnum {
    const UNKNOWN_ERROR = -1; //未知错误
    const SUCCESS = 0; //请求成功
    const PARAMETER_ERROR = 4000; //请求参数错误
    const EMPTY_DATA = 4001; //请求数据为空
    const TOKEN_INVALID = 4002; //token无效
    const TOKEN_EXPIRED = 4003; //token过期
    const LOGIN_FALLED = 4004; //登录失败
    const WORK_ALREADY_START = 4005; //该用户已经上班
    const WORK_ALREADY_END = 4006; //该用户已经下班
    const TASK_START_FAIL = 4007; //任务开始（开工）失败
    const CANNOT_REPORT = 4008; //任务状态不能报产
    const TASK_ALREADY_START = 4009; //任务已经开始或结束
}