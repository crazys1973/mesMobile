<?php
/**
 * Created by 惠达浪
 * Email: crazys@126.com
 * Date: 2018/12/2
 * Time: 15:55
 */

namespace app\api\model;

use think\Model;

class Mesmaterial extends Model {
    protected $hidden = ['create_time', 'update_time'];

    /**
     * 关联mescomponent表
     * @return \think\model\relation\BelongsTo
     */
    public function mescomponent() {
        return $this->belongsTo('Mescomponent', 'mescomponent_id', 'id');
    }

    /**
     * 关联mesworksheet表
     * @return \think\model\relation\BelongsTo
     */
    public function mesworksheet() {
        return $this->belongsTo('Mesworksheet', 'mesworksheet_id', 'id');
    }

    /**
     * 关联material表
     * @return \think\model\relation\BelongsTo
     */
    public function material() {
        return $this->belongsTo('Material','material_id','id');
    }
}