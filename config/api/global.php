<?php
/**
 * Created by 惠达浪
 * Email: crazys@126.com
 * Date: 2018/11/23
 * Time: 16:00
 */

return [
    'token' => [
        //令牌加密字符串
        'salt' => 'ce425277503946c03c94f4b695771c0f',
        //令牌过期时间
        'expire' => 3600 * 24 * 2
    ],
    //消息加密字符串
    'message_salt' => '7257efeded1135c5b16847dac8b8856a',
    //手机端轮播图数量
    'slides_num' => 3,
    //手机端最新文章获取数量
    'news_num' => 5
];