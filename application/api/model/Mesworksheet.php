<?php
/**
 * Created by 惠达浪
 * Email: crazys@126.com
 * Date: 2018/12/1
 * Time: 23:34
 */

namespace app\api\model;

use think\Model;

class Mesworksheet extends Model {
    protected $hidden = ['create_time', 'update_time'];

    /**
     * 关联user表
     *
     * @return \think\model\relation\BelongsTo
     */
    public function user() {
        return $this->belongsTo('User', 'user_id', 'id');
    }

    /**
     * 关联company表
     *
     * @return \think\model\relation\BelongsTo
     */
    public function company() {
        return $this->belongsTo('Company', 'mws_company', 'id');
    }

    /**
     * 关联mesmaterial表
     *
     * @return \think\model\relation\HasMany
     */
    public function mesmaterial() {
        return $this->hasMany('Mesmaterial', 'mesworksheet_id', 'id');
    }

    /**
     * 关联mescomponent表
     *
     * @return \think\model\relation\HasMany
     */
    public function mescomponent() {
        return $this->hasMany('Mescomponent', 'mesworksheet_id', 'id');
    }

    /**
     * 关联mesprocess表
     *
     * @return \think\model\relation\HasMany
     */
    public function mesprocess() {
        return $this->hasMany('Mesprocess', 'mesworksheet_id', 'id');
    }

    public function erpmain() {
        return $this->belongsTo('ErpMain', 'mws_orderno', 'id');
    }
}