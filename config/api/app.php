<?php
/**
 * Created by 惠达浪
 * Email: crazys@126.com
 * Date: 2018/11/21
 * Time: 23:45
 */

// api应用配置
return [
    // 禁止访问模块
    'deny_module_list' => ['common', 'lib'],
    // 默认输出类型
    'default_return_type' => 'json',
    // 异常处理handle类
    'exception_handle' => 'app\lib\exception\ExceptionHandle'
];