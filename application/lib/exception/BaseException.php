<?php
/**
 * Created by 惠达浪
 * Email: crazys@126.com
 * Date: 2018/11/22
 * Time: 0:41
 */

namespace app\lib\exception;

use app\lib\enum\ErrorCodeEnum;
use think\Exception;

/**
 * 自定义异常类的基类
 */
class BaseException extends Exception {
    public $code = 500;
    public $msg = '服务器发生未知错误';
    public $errorCode = ErrorCodeEnum::UNKNOWN_ERROR;

    /**
     * 构造函数，接收一个关联数组
     * @param int $code http状态码
     * @param string $message 错误信息
     * @param int $errorCode 系统全局错误码
     */
    public function __construct($code = null, $message = null, $errorCode = null) {
        $code && $this->code = $code;
        $message && $this->msg = $message;
        $errorCode && $this->errorCode = $errorCode;
    }
}